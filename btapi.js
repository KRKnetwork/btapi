const querystring = require('querystring')
const crypto = require('crypto')
const fetch = require('node-fetch')
const defaults = require('lodash.defaults')

const jBaseConfig = {
  url: 'https://api.bitcoin.de',
  version: 'v1',
  agent: 'btapi',
  key: '',
  secret: '',
  timeoutMS: 20000
}

module.exports = class BTAPI {
  constructor (jConfig) {
    this.config = defaults({}, jConfig, jBaseConfig)
    this.repeatNonce = 0
  }

  get (sMethod, jParams = {}) {
    const sQuery = querystring.stringify(jParams)
    const sURL = this.config.url + '/' + this.config.version + '/' + sMethod + (sQuery === '' ? '' : ('?' + sQuery))
    const sNonce = this._getNonce()
    let sMD5 = 'd41d8cd98f00b204e9800998ecf8427e'

    const sSignature = crypto
      .createHmac('sha256', this.config.secret)
      .update(`GET#${sURL}#${this.config.key}#${sNonce}#${sMD5}`)
      .digest('hex')

    const jOptions = {
      headers: {
        'User-Agent': this.config.agent,
        'X-API-KEY': this.config.key,
        'X-API-NONCE': sNonce,
        'X-API-SIGNATURE': sSignature
      }
    }

    return fetch(sURL, jOptions)
      .then(res => res.json())
      .then(jIn => {
        if (jIn.errors.length > 0) {
          switch (jIn.errors[0].code) {
            case 4:
              console.log('BTAPI Nonce Hit: ' + sMethod + ' (' + sNonce + ')')
              break
            case 6:
              console.log('BTAPI Insufficient Credits: ' + sMethod + ' (' + jIn.credits + ')')
              break
            default:
              console.log(jIn.errors)
          }

          return false
        }

        return jIn
      })
  }

  _getNonce () {
    const iNow = Math.pow(10, 2) * Date.now()

    if (iNow === this.lastNonce) {
      this.repeatNonce++
    } else {
      this.repeatNonce = 0
      this.lastNonce = iNow
    }

    const sOut = (iNow + this.repeatNonce).toString()
    return +sOut.substr(sOut.length - 15)
  }
}
