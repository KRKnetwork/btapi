# BTAPI Client

Basic Wrapper for the Bitcoin.de Trading API

## Installing / Getting started

Installing

```shell
npm i git+https://gitlab.com/KRKnetwork/btapi.git
```

Basic Usage

```js
const BTAPI = require('@krk/btapi')

const btapiClient = BTAPI({ key: '', secret: '' })
```

# Api Reference

- BTAPI Documentation: [https://www.bitcoin.de/de/api/tapi/v1/docu](https://www.bitcoin.de/de/api/tapi/v1/docu)
- BTAPI Keys: [https://www.bitcoin.de/de/userprofile/tapi](https://www.bitcoin.de/de/userprofile/tapi)

# Usage

```js
const BTAPI = require('@krk/btapi')

const btapiClient = BTAPI({ key: '', secret: '' })

// Example Calls
btapiClient
  .get('account')
  .then(jRes => console.log(jRes))

btapiClient
  .get('trades/history', { since_tid: '123456' })
  .then(jRes => {
    if (jRes === false) return

    console.log(jRes)
  })
```

Required Config:

```js
{ key: '', secret: ''}
```

Full Config:

```js
{
  url: 'https://api.bitcoin.de',
  version: 'v1',
  agent: 'btapi',
  key: '',
  secret: '',
  timeoutMS: 20000
}
```

## Developing

Issue List: [https://gitlab.com/KRKnetwork/btapi/issues](https://gitlab.com/KRKnetwork/btapi/issues)

- jConfig Error Message
- Replace node-fetch
- Replace lodash.defaults
